import {Component} from 'react';
var today = new Date();
var time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
class RealTime extends Component {
    constructor(props) {
        super(props);
        this.state = {
            realTime: time,
            colorText: "black"
        }
        
    }
    ChangeColor = () => {
        if(today.getSeconds()%2 === 0) {
            this.setState({
                colorText: "red"
            })
        } else {
            this.setState({
                colorText: "blue"
            })
        }
    }
    render() {
        return (
            <div style={{textAlign: "center", padding: "10%"}}>
                <p style={{fontWeight: "bolder", color: this.state.colorText}}>Hello World!!!</p>
                <p style={{fontWeight: "bolder", color: this.state.colorText}}>It's <span> {this.state.realTime} </span></p>
                <button style={{fontWeight: "bolder", backgroundColor: "Chocolate", padding: "5px 10px"}} onClick={this.ChangeColor}>Change Color</button>
            </div>

        )
    }
}
export default RealTime;